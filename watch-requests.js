//chrome.webRequest.onHeadersReceived.addListener(
browser.webRequest.onHeadersReceived.addListener(
	details => {
		//console.log('hres', details);
		//if (!details || !details.url) return;
		if (!details || !details.documentUrl) return;

		const headers = details.responseHeaders || [];
		const contType = (headers.find(h => h.name.toUpperCase() === 'CONTENT-TYPE') || {value: ''}).value.split(';')[0];
		const contLength = (headers.find(h => h.name.toUpperCase() === 'CONTENT-LENGTH') || {value: 0}).value;
		const domain = new URL(details.documentUrl).hostname;
		console.log('got header response', /*{
			docUrl: details.documentUrl,
			url: domain,
			type: contType,
			length: contLength
		}*/details);

		//retrieve the domain from local storage
		browser.storage.local.get(domain).then(domainVal => {;
		//chrome.storage.local.get(domain, domainVal => {;
			//console.log('res', domainVal);

			//if we haven't seem this domain, init it
			if (!domainVal[domain]) {
				domainVal[domain] = {
					total: 0,
					types: {}
				};
			}

			//add the data for this request
			domainVal[domain].total += contLength*1;
			domainVal[domain][contType] = (domainVal[domain][contType] || 0) + contLength*1;

			//save the domain back
			browser.storage.local.set(domainVal);
			//chrome.storage.local.set(domainVal);
		});
	},
	{urls: ["<all_urls>"]},
	["responseHeaders"]
);

//https://stackoverflow.com/a/14919494
function humanFileSize(bytes) {
    var thresh = 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1)+' '+units[u];
}


