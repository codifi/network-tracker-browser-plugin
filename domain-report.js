function humanFileSize(bytes) {
    var thresh = 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + 'B';
    }
    var units = ['KB','MB','GB','TB','PB','EB','ZB','YB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1)+' '+units[u];
}


if (!('browser' in window)) window.browser = chrome;
//browser.storage.local.get(null).then(o => {
browser.storage.local.get(null, o => {
	//console.log('res', o);
	const sites = Object.keys(o).map(k => ({domain: k, usage: o[k]}))
			.sort((a, b) => a.usage.total - b.usage.total);
	let total = 0;

	sites.forEach(s => {
		total += s.usage.total;
		const row = document.getElementById('table-body').insertRow();
		const domainCell = row.insertCell();
		domainCell.appendChild(document.createTextNode(s.domain));

		const usageCell = row.insertCell();
		usageCell.appendChild(document.createTextNode(humanFileSize(s.usage.total)));
	});

	document.getElementById('total-usage').textContent = humanFileSize(total);
});

