# Network Data Tracker

Simple plugin to help determine how much data is being used by which domains. Some ISPs won't tell you which domains are using your data, so when you're on a capped data allotment, it's valuable information to know. When I was struggling with a data cap, I wrote this plugin to help me track down the culprit domain. 